#include "checkboxdelegate.h"


CheckBoxDelegate::CheckBoxDelegate(QObject *parent)
    : QItemDelegate(parent)
{
}

void CheckBoxDelegate::paint(QPainter *painter,
                             const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    auto value = static_cast<Qt::CheckState>(index.data(Qt::CheckStateRole).toUInt());
    this->drawCheck(painter,option,option.rect,value);
    this->drawFocus(painter,option,option.rect);
}

auto CheckBoxDelegate::editorEvent(
        QEvent *event, QAbstractItemModel *model,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) -> bool
{
    Q_UNUSED(option);
    if ((event->type() == QEvent::MouseButtonRelease) ||
            event->type() == QEvent::MouseButtonDblClick)
    {
        auto *mouse_event = static_cast<QMouseEvent*>(event);
        if (mouse_event->button() != Qt::LeftButton)
        {
            // other mouse button pressed, ignore it
            return false;
        }
        if (event->type() == QEvent::MouseButtonDblClick)
        {
            // mouse dbl click event, bail out of fn
            return false;
        }
    } else if (event->type() == QEvent::KeyPress)
    {
        if (static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space &&
                static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select)
        {
            // other key press event, bail out of fn
            return false;
        } // if it was a space or select, it's fine, will fall through
    } else
    {
        // other kind of rx event, bail out of fn
        return false;
    }

    // presumably a regular click happened
    quint32 ck = index.model()->data(index, Qt::CheckStateRole).toUInt();
    bool oldState = ck > 0;
    bool newState = !oldState;
    Qt::CheckState newCk = newState ? Qt::CheckState::Checked : Qt::CheckState::Unchecked;
    auto stdmodel = qobject_cast<QStandardItemModel*>(model);
    auto item = stdmodel->itemFromIndex(index);
    auto editable = item->isEditable();
    if (editable)
    {
        qDebug() << "model says prev state was: " << ck;
        qDebug() << "checkbox has been set to: " << newCk;
        return model->setData(index, newCk, Qt::CheckStateRole);
    } else
    {
        qDebug() << "not proceeding because editable is false.";
        return false;
    }

}
