#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "timecelldelegate.h"

TimeCellDelegate::TimeCellDelegate(QObject *parent)
{
    Q_UNUSED(parent)
}

auto TimeCellDelegate::createEditor(QWidget *parent,
                                        const QStyleOptionViewItem &option,
                                        const QModelIndex &index) const -> QWidget *
{
    qDebug() << "Creating the editor";
    auto *lineEdit= new QLineEdit(parent);
    lineEdit->setFrame(false);
    QFont mono("SourceCodePro");
    mono.setStyleHint(QFont::Monospace);
    //lineEdit->setFont(mono);


    switch(index.column())
    {
    case Columns::Start:
    {
        lineEdit->setInputMask("99:99:99.000;x");
        return lineEdit;
    } break;
    case Columns::Duration:
    {
        lineEdit->setReadOnly(true);
        return lineEdit;
    } break;
    default:
        return QStyledItemDelegate::createEditor(parent,option,index);
    }
}

void TimeCellDelegate::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
    QVariant vvalue = index.model()->data(index, Qt::EditRole);
    QString value;
    switch(index.column())
    {
    case Columns::Start:
    case Columns::Duration:
        value = vvalue.toTime().toString("hh:mm:ss.zzz");
        break;
    default:
        qWarning() << "Got bad column.";
        return;
    }

    auto *lineEdit = static_cast<QLineEdit*>(editor);
    lineEdit->setText(value);
}

void TimeCellDelegate::setModelData(QWidget *editor,
                                    QAbstractItemModel *model,
                                    const QModelIndex &index) const
{
    switch(index.column())
    {
    case Columns::Start:
    {
        qDebug() << "Setting model data for start time?";
        auto *lineEdit = static_cast<QLineEdit*>(editor);
        QString value = lineEdit->text();
        qDebug() << "Line edit had text: " << value;
        QTime tvalue = QTime::fromString(value, "hh:mm:ss.zzz");
        model->setData(index,tvalue,Qt::EditRole);
        break;
    }
    }
}

void TimeCellDelegate::updateEditorGeometry(QWidget *editor,
                                            const QStyleOptionViewItem &option,
                                            const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}

void TimeCellDelegate::paint(QPainter *painter,
                                     const QStyleOptionViewItem &option,
                                     const QModelIndex &index) const
{
    switch(index.column())
    {
    case Columns::Duration:
    case Columns::Start:
    {
        QStyledItemDelegate::paint(painter, option, index);
        break;
    }
    }
}

auto TimeCellDelegate::displayText(const QVariant &value, const QLocale &locale) const -> QString
{
    Q_UNUSED(locale)
    if (value.canConvert<QTime>())
    {
        QString result = value.toTime().toString("hh:mm:ss.zzz");
        return result;
    }
    return "";
}

