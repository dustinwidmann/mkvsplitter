#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "timecelldelegate.h"
#include "checkboxdelegate.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct Job
{
    QStringList task;
    QTime startTime;
    QTime duration;
    QTime elapsedBefore;
    QString outfile;
};

QString getExecPath(QString prog);


/*!
 * \brief MainWindow::timeSubtraction
 *
 * \details subtracts one time from another, yields another object of the same
 * type (hours,minutes,seconds, as opposed to just seconds or msecs)
 * \param t1 - time 1
 * \param t2 - time 2
 * \return - time difference
 */
inline QTime timeSubtraction(QTime t1, QTime t2)
{
    float msecs;
    if (t2>t1)
        msecs = t1.msecsTo(t2);
    else
        msecs = t2.msecsTo(t1);
    QTime difference = QTime(0,0,0,0).addMSecs(msecs);


    return difference;
}
/*!
 * \brief MainWindow::timeAddition
 *
 * \details adds one time from another, yields another object of the same
 * type (hours,minutes,seconds, as opposed to just seconds or msecs)
 * \param t1 - time 1
 * \param t2 - time 2
 * \return - time sum
 */
inline QTime timeAddition(QTime t1, QTime t2)
{
    int msecs1 = QTime(0,0,0,0).msecsTo(t1);
    int msecs2 = QTime(0,0,0,0).msecsTo(t2);

    QTime added = QTime(0,0,0,0).addMSecs(msecs1+msecs2);

    return added;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void readChapterInformation();
    void validateInput();
    void setFile(QString file);

private slots:
    void showInfilePicker();
    void showOutdirPicker();
    void showPreview();
    void startSplitting();
    void abortSplitting();
    void exit();
    void showAboutDialog();
    void handleBasenameChanged(const QString &arg1);

    void handleSplitProcHasBytes();
    void handleSplitProcFinished(int, QProcess::ExitStatus);
    void handleSplitProcError(QProcess::ProcessError);
    void modelItemChanged(QStandardItem*);
    void modelDataChanged(const QModelIndex &topLeft,
                              const QModelIndex &bottomRight,
                              const QVector<int> &roles);
//    void on_table_clicked(const QModelIndex&);
private:
    Ui::MainWindow *_ui;
    QString _infile;
    QTime _infileDuration;
    QTime _jobDuration;
    bool _populated;
    QProcess _splitProcess;
    Job _currentJob;

    QStandardItemModel _model;
    TimeCellDelegate _startDelegate;
    TimeCellDelegate _durationDelegate;
    CheckBoxDelegate _splitboxDelegate;
    CheckBoxDelegate _keepboxDelegate;

    QQueue<Job> _taskList;

    void _processQueue();
    void _updateKeepStyle();
    void _updateTimes();
    void _determineJobTimings(
            QList<QTime>& starts,
            QList<QTime>& durations,
            QList<QTime>& elapsedBefore);
    void _buildTaskList(
            const QList<QTime>& starts,
            const QList<QTime>& durations,
            const QList<QTime>& elapsedBefore);
    //!
    //! _getSplitChecked
    //!
    //! simple function gets the value of the "split" checkbox at "row"
    //! \param row - row to check
    //! \return true if checked, false otherwise
    //!
    inline bool _getSplitChecked(int row)
    {
        return _model.item(row,Columns::Splitbox)->data(Qt::CheckStateRole).toBool();
    }
    //!
    //! _getKeepChecked
    //!
    //! simple function gets the value of the "keep" checkbox at "row"
    //! \param row - row to check
    //! \return true if checked, false otherwise
    //!
    inline bool _getKeepChecked(int row)
    {
        return _model.item(row,Columns::Keepbox)->data(Qt::CheckStateRole).toBool();
    }
    //!
    //! _setKeepChecked
    //!
    //! simple function sets the value of the "keep" checkbox at "row" to "state"
    //! \param row - row to set
    //! \param state - state to use
    //!
    inline void _setKeepChecked(int row, bool state)
    {
        QModelIndex idx = _model.index(row,Columns::Keepbox,QModelIndex());
        QVariant cs = state ? Qt::CheckState::Checked : Qt::CheckState::Unchecked;
        _model.setData(idx,cs,Qt::CheckStateRole);
    }
    //!
    //! \brief _setSplitChecked
    //!
    //! simple function sets the value of the "split" checkbox at "row" to "state"
    //! \param row - row to set
    //! \param state - state to use
    //!
    inline void _setSplitChecked(int row, bool state)
    {
        QModelIndex idx = _model.index(row,Columns::Splitbox,QModelIndex());
        QVariant cs = state ? Qt::CheckState::Checked : Qt::CheckState::Unchecked;
        _model.setData(idx,cs,Qt::CheckStateRole);
    }
};
#endif // MAINWINDOW_H
