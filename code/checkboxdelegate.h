#ifndef CHECKBOXDELEGATE_H
#define CHECKBOXDELEGATE_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

class CheckBoxDelegate : public QItemDelegate

{
    Q_OBJECT
public:
    CheckBoxDelegate(QObject *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;
    bool editorEvent(QEvent *event,
                     QAbstractItemModel *model,
                     const QStyleOptionViewItem &option,
                     const QModelIndex &index) override;
};

#endif // CHECKBOXDELEGATE_H
