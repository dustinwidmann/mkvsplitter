#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtDebug>
#include <QtGlobal>

#include <cmath>

#ifndef COLUMNS
#define COLUMNS

#endif

//!
//! MainWindow::MainWindow
//!
//! Main class for the MkvSplitter application
//! \param parent - used by Qt for garbage collection
//!
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow),
    _populated(false),
    _model(0,4,this),
    _startDelegate(this),
    _durationDelegate(this),
    _splitboxDelegate(this),
    _keepboxDelegate(this)
{
    _ui->setupUi(this);

    _ui->outdirButton->setIcon(style()->standardIcon(QStyle::SP_DirIcon));
    _ui->outdirButton->setText("");

    _ui->infileButton->setIcon(style()->standardIcon(QStyle::SP_FileIcon));
    _ui->infileButton->setText("");

    _ui->stackedWidget->setCurrentIndex(0);

    _model.setHorizontalHeaderItem(0, new QStandardItem("Split"));
    _model.setHorizontalHeaderItem(1, new QStandardItem("Start Time"));
    _model.setHorizontalHeaderItem(2, new QStandardItem("Length"));
    _model.setHorizontalHeaderItem(3, new QStandardItem("Chapter Name"));
    _model.setHorizontalHeaderItem(4, new QStandardItem("Keep"));

    _ui->table->setModel(&_model);
    _ui->table->setItemDelegateForColumn(Columns::Start,&_startDelegate);
    _ui->table->setItemDelegateForColumn(Columns::Duration,&_durationDelegate);
    _ui->table->setItemDelegateForColumn(Columns::Keepbox,&_keepboxDelegate);
    _ui->table->setItemDelegateForColumn(Columns::Splitbox,&_splitboxDelegate);
    _ui->table->setShowGrid(false);
    _ui->table->horizontalHeader()->setVisible(true);
    _ui->table->verticalHeader()->setVisible(true);
    _ui->table->setSelectionMode(QAbstractItemView::SingleSelection);
    _ui->table->setAlternatingRowColors(false);


    _ui->previewButton->setEnabled(false);
    _ui->splitButton->setEnabled(false);

    // connect the important signals and slots
    connect(&_splitProcess, &QProcess::readyReadStandardError,
            this, &MainWindow::handleSplitProcHasBytes);
    connect(&_splitProcess, &QProcess::readyReadStandardOutput,
            this, &MainWindow::handleSplitProcHasBytes);
//    connect(&_splitProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
//            this, SLOT(on_splitProcess_finished(int,QProcess::ExitStatus)));
    connect(&_splitProcess, &QProcess::finished,
            this, &MainWindow::handleSplitProcFinished);
    connect(&_splitProcess, &QProcess::errorOccurred,
            this, &MainWindow::handleSplitProcError);
    connect(&_model, &QStandardItemModel::itemChanged,
            this, &MainWindow::modelItemChanged);
    connect(&_model, &QStandardItemModel::dataChanged,
            this, &MainWindow::modelDataChanged);

    // connect the other interface-related signals and slots
    connect(_ui->outdirButton, &QPushButton::clicked,
            this, &MainWindow::showOutdirPicker);
    connect(_ui->infileButton, &QPushButton::clicked,
            this, &MainWindow::showInfilePicker);
    connect(_ui->actionOpen, &QAction::triggered,
            this, &MainWindow::showInfilePicker);
    connect(_ui->splitButton, &QPushButton::clicked,
            this, &MainWindow::startSplitting);
    connect(_ui->stopButton, &QPushButton::clicked,
            this, &MainWindow::abortSplitting);
    connect(_ui->basename, &QLineEdit::textChanged,
            this, &MainWindow::handleBasenameChanged);
    connect(_ui->action_About, &QAction::triggered,
            this, &MainWindow::showAboutDialog);
    connect(_ui->actionExit, &QAction::triggered,
            this, &MainWindow::exit);
    connect(_ui->previewButton, &QPushButton::clicked,
            this, &MainWindow::showPreview);
}

//!
//! MainWindow::~MainWindow
//!
//! MkvSplitter destructor function
//!
MainWindow::~MainWindow()
{
    delete _ui;
}



//!
//! MainWindow::_determineJobTimings
//!
//! Loops through the data model getting the start times, durations, and such
//! for each split segment. Will be called just prior to splitting.
//! \param[out] starts
//! \param[out] durations
//! \param[out] elapsedBefore
//!
void MainWindow::_determineJobTimings(
        QList<QTime>& starts,
        QList<QTime>& durations,
        QList<QTime>& elapsedBefore)
{
    _jobDuration = QTime(0,0,0,0);

    // take note of which rows have been marked for split and keep
    for (int row = 0 ; row < _model.rowCount(); ++row) {
        qDebug() << "Processing row " << row;
        bool splitChecked = _getSplitChecked(row);
        bool keepChecked = _getKeepChecked(row);
        qDebug() << "row: " << row << " ; split: " << splitChecked << " ; keep: " << keepChecked;
        if (splitChecked == true)
        {
            qDebug() << "Splitting at row " << row;
            if (keepChecked == true)
            {
                qDebug() << "Keeping segment starting at row " << row;
                starts << _model.item(row,Columns::Start)->data(Qt::DisplayRole).toTime();
                durations << _model.item(row,Columns::Duration)->data(Qt::DisplayRole).toTime();
                elapsedBefore.append(_jobDuration);
                _jobDuration = timeAddition(_jobDuration, durations[durations.length()-1]);
            }
        } else
        {
            if (keepChecked == true)
            {
                qDebug() << "Keeping segment started prior to row " << row;
                durations[durations.length()-1] =
                    timeAddition(durations[durations.length()-1],
                        _model.item(row,Columns::Duration)->data(Qt::DisplayRole).toTime());
                _jobDuration = timeAddition(_jobDuration, durations[durations.length()-1]);
            }
        }
    }
}

//!
//! MainWindow::_buildTaskList
//!
//! Takes the timing information from _determineJobTimings and user supplied
//! infile, outdir, and basename to make a list of ffmpeg commands to be run.
//! It adds them to the member variable _taskList
//! \param[in] starts
//! \param[in] durations
//! \param[in] elapsedBefore
//!
void MainWindow::_buildTaskList(
        const QList<QTime>& starts,
        const QList<QTime>& durations,
        const QList<QTime>& elapsedBefore)
{
    // make a pairwise list of start and endpoint in ffmpeg format
    QString infile = _ui->infile->text();
    QString outdir = _ui->outdir->text();
    QString basename = _ui->basename->text();

    // get timecodes associated with ones that are to be kept and the
    // associated durations
    for (int row = 0; row < starts.length(); ++row) {
        QString start = starts[row].toString("hh:mm:ss.zzz");
        QString duration = durations[row].toString("hh:mm:ss.zzz");

        QString outname;
        QTextStream(&outname) << outdir << "/" << basename << "_" << row << ".mkv";
        QStringList args;
        args
                << "-loglevel" <<"0"
                << "-stats"
                << "-i" << infile
                << "-map" <<  "0"
                << "-c" << "copy"
                << "-ss" << start
                << "-t" << duration;
        qDebug() << "From start " << start << " to " << duration;
        args << "-y";
        args << outname;
        Job j;
        j.startTime = starts[row];
        j.duration = durations[row];
        j.elapsedBefore = elapsedBefore[row];
        j.task = args;
        j.outfile = outname;
        qDebug().noquote() << "Adding job: "
                 << "\nstart: " << j.startTime.toString("hh:mm:ss.zzz")
                 << "\nduration: " << j.duration.toString("hh:mm:ss.zzz")
                 << "\nelapsedBefore: " << j.elapsedBefore.toString("hh:mm:ss.zzz")
                 << "\ntask: " << j.task
                 << "\noutfile: " << j.outfile;



        _taskList.enqueue(j);
    }
}

//!
//! MainWindow::startSplitting
//!
//! Event handler for splitButton clicked event. Disables other UI
//! elements, builds a text command to be executed by mkvmerge or ffmpeg,
//! changes panes to the split in progress view, and then executes the command
//! with an external system call.
//!
void MainWindow::startSplitting()
{
    qDebug() << "##########";
    qDebug() << "startSplitting() called";

    _ui->stackedWidget->setCurrentIndex(1);
    _ui->menubar->setEnabled(false);
    _ui->table->setEnabled(false);
    _ui->outdirButton->setEnabled(false);
    _ui->infileButton->setEnabled(false);
    this->setCursor(Qt::WaitCursor);

    QList<QTime> starts;
    QList<QTime> durations;
    QList<QTime> elapsedBefore;
    _determineJobTimings(starts,durations,elapsedBefore);
    _buildTaskList(starts,durations,elapsedBefore);
    auto msecs = QTime(0,0,0,0).msecsTo(_jobDuration);
    _ui->progressBar->setMaximum(msecs);
    if (!_taskList.empty())
        _processQueue();
    qDebug() << "startSplitting() handler finished.";
    qDebug() << "##########";
}

//!
//! MainWindow::_processQueue
//!
//! Moves to the next item in the _taskList and executes it.
//! If the task list is already empty then it returns immediately.
//!
//!
void MainWindow::_processQueue() {
    qDebug() << "prepared to create new process";
    _splitProcess.setProcessChannelMode(QProcess::MergedChannels);

    QString program = getExecPath("ffmpeg");
    qDebug() << "ffmpeg path is: " << program;
    if (_taskList.isEmpty())
    {
        qDebug() << "The queue was empty ... this wasn't supposed to happen!";
        return;
    }
    _currentJob = _taskList.dequeue();

    _ui->progressBar->setValue(QTime(0,0,0,0).msecsTo(_currentJob.elapsedBefore));
    QStringList args = _currentJob.task;

    qDebug() << "prepared to start new process";
    qDebug() << "ffmpeg command is: " << program << " " << args.join(" ");

    _ui->fileWriting->setEnabled(false);
    _ui->fileWriting->setText(_currentJob.outfile);

    _splitProcess.setProgram(program);
    _splitProcess.setArguments(args);
    _splitProcess.start();
}

void MainWindow::modelItemChanged(QStandardItem *item)
{
//    qDebug() << "itemChanged() called";
    if (_populated)
    {
        if (item->index().column() == Columns::Start)
        { // then update all of the durations
            _updateTimes();
        }
        if (item->index().column() == Columns::Splitbox
                || item->index().column() == Columns::Keepbox)
        {
            bool segmentKeep = true;
            for (int row = 0 ; row < _model.rowCount(); row++)
            {
                bool split = _getSplitChecked(row);
                bool keep = _getKeepChecked(row);
                if (split)
                { // start of a new segment
                    segmentKeep = keep;
                } else
                { // copy check state of split line to other lines in segment
                    _setKeepChecked(row, segmentKeep);
                }
            }
            _updateKeepStyle();
        }
    }
}

void MainWindow::modelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    Q_UNUSED(roles);
    Q_UNUSED(bottomRight);
//    qDebug() << "dataChanged()";
    // assume it's only one item, so topLeft == bottomRight
    QStandardItem *item = _model.itemFromIndex(topLeft);
    modelItemChanged(item);
}

//!
//! \brief MainWindow::_updateKeepStyle
//!
//! This function sets background color for unkept sections, and also deals with
//! alternating background (by section) for kept sections.
//!
void MainWindow::_updateKeepStyle()
{
    int numChapters = _model.rowCount();
    auto palette = _ui->table->palette();
    auto stdBg = palette.color(QPalette::ColorGroup::Normal,QPalette::ColorRole::Base);
    auto altBg = palette.color(QPalette::ColorGroup::Normal,QPalette::ColorRole::AlternateBase);
    auto disBg = palette.color(QPalette::ColorGroup::Disabled,QPalette::ColorRole::Base);
    auto stdTxt = palette.color(QPalette::ColorGroup::Normal,QPalette::ColorRole::Text);
    auto altTxt = palette.color(QPalette::ColorGroup::Normal,QPalette::ColorRole::Text);
    auto disTxt = palette.color(QPalette::ColorGroup::Disabled,QPalette::ColorRole::Text);

    bool toggle = false;

    auto curRowTxtColor = altTxt;
    auto curRowBgColor = altBg;

    for (int curChapter = 0; curChapter < numChapters; ++curChapter)
    {
        auto splitIdx = _model.index(curChapter,Columns::Splitbox);
        auto startIdx = _model.index(curChapter,Columns::Start);
        auto lengthIdx = _model.index(curChapter,Columns::Duration);
        auto chapterNameIdx = _model.index(curChapter,Columns::ChapterName);
        auto keepIdx = _model.index(curChapter,Columns::Keepbox);

        QList<QModelIndex> idxList;
        idxList << splitIdx <<startIdx << lengthIdx << chapterNameIdx
                 << keepIdx;

        bool keepChecked = _getKeepChecked(curChapter);
        bool splitChecked = _getSplitChecked(curChapter);
        if (splitChecked && keepChecked)
        {
            qDebug() << "both checked. Cur row is" << curChapter << "and toggle is" << toggle;
            if (toggle)
            {
                curRowBgColor = altBg;
                curRowTxtColor = altTxt;
            } else
            {
                curRowBgColor = stdBg;
                curRowTxtColor = stdTxt;
            }
            toggle = !toggle;
        } else if (!keepChecked)
        {
            curRowBgColor = disBg;
            curRowTxtColor = disTxt;
        }

        for (auto idx : idxList)
        {
            _model.setData(idx,curRowBgColor,Qt::BackgroundRole);
            _model.setData(idx,curRowTxtColor,Qt::ForegroundRole);
        }
    }
}

//!
//! MainWindow::_updateTimes
//!
//! If a duration, start time, etc has been altered, this will update the
//! other times accordingly
//!
void MainWindow::_updateTimes()
{
    int numChapters = _model.rowCount();
    for (int curChapter = 0; curChapter < numChapters; ++curChapter) {
        QModelIndex splitIdx, startIdx, lengthIdx, chapterNameIdx, keepIdx;
        splitIdx = _model.index(curChapter,Columns::Splitbox);
        startIdx = _model.index(curChapter,Columns::Start);
        lengthIdx = _model.index(curChapter,Columns::Duration);
        chapterNameIdx = _model.index(curChapter,Columns::ChapterName);
        keepIdx = _model.index(curChapter,Columns::Keepbox);
        QStandardItem *splitItem = _model.itemFromIndex(splitIdx);
        QStandardItem *startItem = _model.itemFromIndex(startIdx);
        QStandardItem *lengthItem = _model.itemFromIndex(lengthIdx);
        QStandardItem *chapterNameItem = _model.itemFromIndex(chapterNameIdx);
        QStandardItem *keepItem = _model.itemFromIndex(keepIdx);

        QList<QStandardItem*> itemList;
        itemList << splitItem <<startItem << lengthItem << chapterNameItem
                 << keepItem;

        QTime duration;
        QTime start;
        QTime stop;

        start = _model.data(startIdx).toTime();
        if (curChapter < (numChapters-1))
        { // not the last chapter
            QModelIndex nextIdx = _model.index(curChapter+1,
                                               Columns::Start,QModelIndex());
            stop = _model.data(nextIdx).toTime();
        } else
        {
            stop = _infileDuration;
        }
        duration = timeSubtraction(start,stop);

        _model.setData(lengthIdx,duration);
        qDebug() << "Chapter " << curChapter << ": "
                 << duration.toString("hh:mm:ss.zzz");
    }
}

//!
//! \brief MainWindow::abortSplitting
//!
void MainWindow::abortSplitting()
{
    _taskList.clear();
    _splitProcess.terminate();

    statusBar()->showMessage("Cancelled split operation");

    _ui->stackedWidget->setCurrentIndex(0);
    _ui->menubar->setEnabled(true);
    _ui->table->setEnabled(true);
    _ui->outdirButton->setEnabled(true);
    _ui->infileButton->setEnabled(true);
    this->setCursor(Qt::ArrowCursor);
}

/*!
 * \brief MainWindow::getExecPath
 *
 * \details Tries to get the executable path for a program name. For example,
 * I might expect "sh" to resolve to "/usr/bin/sh"
 * \param prog - name of the program whose path we want
 * \return full path for that program or an empty string if it couldn't be found.
 */
auto getExecPath(QString prog) -> QString {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!env.contains("PATH")) {
        qDebug() << "Env doesn't include path? That doesn't even make sense.";
        return QString("");
    } else {
        qDebug() << "PATH is: " << env.value("PATH");
        QStringList pathlist = env.value("PATH").split(':',
                Qt::SkipEmptyParts);
        for (auto const &path : pathlist) {
            QDirIterator it(path, QStringList() << prog, QDir::NoFilter,
                    QDirIterator::Subdirectories);
            while (it.hasNext()) {
                QFileInfo f(it.next());
                if (f.isExecutable()) {
                    qDebug() << "Have candidate executeable: "
                             << f.absoluteFilePath();
                    return f.absoluteFilePath();
                }
            }
        }
        return QString("");
    }
}

void MainWindow::readChapterInformation()
{
    QString ffprobe = getExecPath("ffprobe");
    QProcess proc;
    QStringList args;
    args << "-i" << this->_infile
          << "-show_chapters"
          << "-print_format" << "json";
    proc.start(ffprobe,args);
    proc.waitForFinished();

    QByteArray extracted = proc.readAllStandardOutput();

    if (proc.exitStatus() != QProcess::NormalExit)
    {
        qDebug() << "ffprobe exit status: ";
        qDebug() << "input file: " << this->_infile;
        qDebug() << "exit code: " << proc.exitCode();
        qDebug() << "application output:";
        qDebug() << extracted;
        return;
    }

    QJsonDocument json = QJsonDocument::fromJson(extracted);
    QVariantMap vmap = json.toVariant().toMap();
    QVariantList chapters = vmap["chapters"].toList();
    int curChapter = 0;
    _model.setRowCount(chapters.size());
    for (const auto & chvariant : chapters)
    {
        // parse the file
        QVariantMap chmap = chvariant.toMap();
        QVariantMap tags = chmap["tags"].toMap();
        QString name = tags["title"].toString();
        double start_time = chmap["start_time"].toDouble();
        double end_time = chmap["end_time"].toDouble();
        QTime zero(0,0,0);
        QTime start;
        start = zero.addMSecs(start_time*1000);
        QTime end;
        end = zero.addMSecs(end_time*1000);
        QTime duration = timeSubtraction(start,end);


        // populate the model
        QModelIndex index = _model.index(curChapter,Columns::ChapterName,QModelIndex());
        _model.setData(index,name,Qt::EditRole);

        index = _model.index(curChapter,Columns::Splitbox,QModelIndex());
        auto *chkbox = new QStandardItem();
        chkbox->setFlags(chkbox->flags() | Qt::ItemIsUserCheckable);
        _model.setItem(curChapter,Columns::Splitbox,chkbox);
        _setSplitChecked(curChapter,false);

        index = _model.index(curChapter,Columns::Keepbox,QModelIndex());
        auto *keepbox = new QStandardItem();
        keepbox->setFlags(keepbox->flags() | Qt::ItemIsUserCheckable);
        _model.setItem(curChapter,Columns::Splitbox,keepbox);
        _setKeepChecked(curChapter,true);

        index = _model.index(curChapter,Columns::Start,QModelIndex());
        _model.setData(index,start);

        index = _model.index(curChapter,Columns::Duration,QModelIndex());
        _model.setData(index,duration);

        // increment the chapter
        curChapter++;
    }

    auto firstSplitIdx = _model.index(0,Columns::Splitbox);
    auto firstStartIdx = _model.index(0,Columns::Start);
    auto splitItem = _model.itemFromIndex(firstSplitIdx);
    auto startItem = _model.itemFromIndex(firstStartIdx);
    if (splitItem)
    { // disable splitting on the first row/chapter
        _setSplitChecked(0,true);
        splitItem->setEnabled(false);
        splitItem->setEditable(false);
    }
    if (startItem) // was previously just startItem 2021-05-18
    { // disable editing of first timestamp
        startItem->setEditable(false);
    }

    _updateTimes();
    _populated = true;
    _ui->table->resizeColumnsToContents();

}

//!
//! MainWindow::showOutdirPicker
//!
//! The onclick event handler for the outdir button. calls up a dialog
//! so the outdir can be selected, validates it, and sets the text in the outdir
//! text box.
//!
void MainWindow::showOutdirPicker()
{
    qDebug() << "##########";
    qDebug() << "on_outdirButton_clicked() called";

    //QFileDialog::Options options;
    QString filter = "";
    QString outdir;
    qDebug() << "'" << outdir.trimmed() << "'";
    auto tmpPaths = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
    QFileDialog dialog = QFileDialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly);
    dialog.setWindowTitle("select the output file directory");
    dialog.setDirectory(tmpPaths.first());


    while (outdir == "") {
        dialog.exec();
        outdir = dialog.selectedFiles().first();
    }

    _ui->outdir->setText(outdir);
    this->validateInput();

    qDebug() << "on_outdirButton_clicked() handler finished.";
    qDebug() << "##########";
}

//!
//! MainWindow::showPreview
//!
//! Show a video preview with mpv
//!
void MainWindow::showPreview()
{
    qDebug() << "##########";
    qDebug() << "showPreview() called";

    int current_row = _ui->table->currentIndex().row();
    QTime timeStart = _model.item(current_row,Columns::Start)->data(Qt::DisplayRole).toTime();

    QString mpv = getExecPath("mpv");
    QStringList args;
    args << "--osd-fractions"
         << "--osd-level=2"
         << "--pause"
         << "--start=" + timeStart.toString("hh:mm:ss.zzz")
         << _ui->infile->text();

    qDebug() << "got args? " << args;
    auto *proc = new QProcess(this);
    proc->setProgram(mpv);
    proc->setArguments(args);
    proc->startDetached();

    qDebug() << "showPreview() handler finished.";
    qDebug() << "##########";

}

//!
//! \brief MainWindow::setFile
//! \param file
//!
void MainWindow::setFile(QString file)
{
    qDebug() << "Set the file that we're dealing with.";
    qDebug() << "setting the infile to " << file;
    _ui->infile->setText(file);
    this->_infile = file;
    qDebug() << "Text set";
    QFileInfo info(file);
    QString path = info.dir().absolutePath();
    _ui->outdir->setText(path);
    _ui->basename->setText(info.baseName());

    this->readChapterInformation();
    qDebug() << "chapter information parsed";
    this->validateInput();
    qDebug() << "input validated";
}

/*!
 * \brief MainWindow::showInfilePicker
 *
 * \details handler for clicking on the File->Open in the menu bar
 */
void MainWindow::showInfilePicker()
{
    qDebug() << "##########";
    qDebug() << "on_actionOpen_triggered() called";

    //QFileDialog::Options options;
    QString filter = "Matroska Containers (*.mkv *.mka *.mks)";
    this->_infile = "";
    while(this->_infile == "") {
        auto tmpPaths = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
        this->_infile = QFileDialog::getOpenFileName(this,
                        "Open an MKV file.", tmpPaths.first(),
                        filter,
                        &filter,
                        QFileDialog::DontConfirmOverwrite);
    }
    this->setFile(_infile);
}

//!
//! MainWindow::exit
//!
//! should close the application properly
//!
void MainWindow::exit()
{
    this->close();
}

//!
//! MainWindow::showAboutDialog
//!
//! Show the about dialog
//!
void MainWindow::showAboutDialog()
{
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("About mkvsplitter");
    msgBox.setTextFormat(Qt::RichText);
    msgBox.setText(
                "A simple program for splitting MKV files based on chapter "
                "information.<br><br>Repository:"
                "<a href='https://gitlab.com/dustinwidmann/mkvsplitter'>"
                "mkvsplitter on GitLab</a> "
                "<br><br>Author: Dustin Widmann <dw2zq@virginia.edu>");
    msgBox.setModal(true);
    msgBox.exec();

}

/*!
 * \brief MainWindow::validateInput
 *
 * \details validates form inputs
 */
void MainWindow::validateInput()
{
   if (_ui->infile->text() != "" &&
           _ui->outdir->text() != "" &&
           _ui->basename->text() != "")
       _ui->splitButton->setEnabled(true);
   else
       _ui->splitButton->setDisabled(true);

   if (_ui->infile->text() != "")
       _ui->previewButton->setEnabled(true);
   else
       _ui->previewButton->setDisabled(true);
}

//!
//! \brief MainWindow::handleBasenameChanged
//!
//! Handler for basename textbox edit event, triggers input validation if it
//! isn't empty.
//! \param arg1
//!
void MainWindow::handleBasenameChanged(const QString &arg1)
{
   if (arg1 != "") {
       this->validateInput();
   }
}

/*!
 * \brief MainWindow::splitProgress
 *
 * \details Callback function to display progress when splitting operation
 * is in progress. Parses the input and ;updates the progress bar.
 */
void MainWindow::handleSplitProcHasBytes()
{
    QTime zero = QTime(0,0,0,0);

    while(_splitProcess.bytesAvailable()) {
        QByteArray line = _splitProcess.readLine();
        qDebug() << line;

        QRegularExpression frame_re(R"(.*?time=(\d+).(\d+).(\d+).(\d+).*)");
        //qDebug() << " validity of expression: " << frame_re.isValid();
        QRegularExpressionMatch frame_match = frame_re.match(line);
        if (frame_match.hasMatch()) {
            int hour = frame_match.captured(1).toInt();
            int minute = frame_match.captured(2).toInt();
            int second = frame_match.captured(3).toInt();
            int msec = frame_match.captured(4).toInt();
            QTime time = QTime(hour,minute,second,msec);
            qDebug() << "Progress: ## " << time.toString("hh:mm:ss.zzz") << " ## "
                     << "to elapsed: ## " << _currentJob.elapsedBefore.toString("hh:mm:ss.zzz") << " ## ";
            time = timeAddition(time,_currentJob.elapsedBefore);
            qDebug() << "Becomes: ## " << time.toString("hh:mm:ss.zzz");
            auto msecs = zero.msecsTo(time);
           _ui->progressBar->setValue(msecs);
        }
        QApplication::processEvents();
    }
}


/*!
 * \brief MainWindow::splitFinished
 *
 * \details Callback function to set things to rights after splitting
 * operation is completed. Re-enables form elements and buttons, changes the
 * pane back to one where things can be input.
 * \param ecode - exit code for the split process
 * \param estat - exit status for the split process
 */
void MainWindow::handleSplitProcFinished(int ecode, QProcess::ExitStatus estat)
{
    if (estat ==  QProcess::ExitStatus::CrashExit) {
        qDebug() << "split operation crashed, exit code  " << ecode;
        statusBar()->showMessage("split operation crashed, exit code " + QString::number(ecode));
    } else {
        qDebug() << "Split Operation didn't crash, exit code " << ecode;
        statusBar()->showMessage("split operation finished with status " + QString::number(ecode));
    }


    if (!_taskList.isEmpty()) {
       _processQueue();
    } else {
       _ui->stackedWidget->setCurrentIndex(0);
       _ui->menubar->setEnabled(true);
       _ui->table->setEnabled(true);
       _ui->outdirButton->setEnabled(true);
       _ui->infileButton->setEnabled(true);
       this->setCursor(Qt::ArrowCursor);
       statusBar()->showMessage("file split complete");
    }
}

//!
//! \brief MainWindow::handleSplitProcError
//!
//! Print debug information about the split process error
//! \param e - unused
//!
void MainWindow::handleSplitProcError(QProcess::ProcessError e)
{
    Q_UNUSED(e);
    QProcess *source = qobject_cast<QProcess*>(sender());
    qDebug()
            << "QProcess had an error message: "
            << source->error();
    qDebug()
            << "It tried to run this program with these arguments: "
            << source->program()
            << source->arguments();
}

