#ifndef TIME_CELL_DELEGATE_H
#define TIME_CELL_DELEGATE_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

enum Columns
{
    Splitbox=0,
    Start=1,
    Duration=2,
    ChapterName=3,
    Keepbox=4
};

class TimeCellDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    TimeCellDelegate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                              const QModelIndex &index) const override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;
    QString displayText(const QVariant &value,
                     const QLocale &locale) const override;
};

#endif // TIMEEDITDELEGATE_H
