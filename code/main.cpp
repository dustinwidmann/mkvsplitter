#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "mainwindow.h"

auto main(int argc, char *argv[]) -> int
{
    QApplication a(argc, argv);
    MainWindow m;

    QCommandLineParser parser;
    parser.addPositionalArgument("file", QCoreApplication::translate("main", "file so split"));
    parser.process(a);
    QStringList positionalArguments = parser.positionalArguments();
    if (!positionalArguments.isEmpty())
    {
        QString relPath = positionalArguments.first();
        QFileInfo info(relPath);
        QString absPath = info.absoluteFilePath();
        if (info.isReadable())
        {
            m.setFile(absPath);
        } else
        {
            qDebug() << "file " << relPath << " doesn't exist or isn't readable.";
        }
    }

    m.show();


    return a.exec();
}
